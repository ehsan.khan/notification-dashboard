import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import { Button } from 'antd';
import { Notify } from './NotifyComponent/Notify';
import { useState } from 'react';

function App() {
  const [clicked,setClicked]=useState(false);
  const data =[
    {
      name:'Will Hargraves',
      address:'602 Gonzales Rd.Haines City, FL 33844',
      issue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque.'
    },
    {
      name:'Ruth Reid',
      address:'76 Lake St.Montgomery, AL 36109',
      issue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque.'
    },
    {
      name:'Ella Daniels',
      address:'7950 Shore St.Grand Island, NE 68801',
      issue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque.'
    },
    {
      name:'Elliot Thompson',
      address:'143 Hall AvenueHendersonville, NC 28792',
      issue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque.'
    },
    {
      name:'Sharon Allen',
      address:'7640 Gates StreetCornelius, NC 28031',
      issue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque.'
    },{
      name:'Will Hargraves',
      address:'602 Gonzales Rd.Haines City, FL 33844',
      issue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque.'
    },
    {
      name:'Ruth Reid',
      address:'76 Lake St.Montgomery, AL 36109',
      issue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque.'
    },
    {
      name:'Ella Daniels',
      address:'7950 Shore St.Grand Island, NE 68801',
      issue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque.'
    },
    {
      name:'Elliot Thompson',
      address:'143 Hall AvenueHendersonville, NC 28792',
      issue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque.'
    },
    {
      name:'Sharon Allen',
      address:'7640 Gates StreetCornelius, NC 28031',
      issue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque.'
    }
  ]
 
  return (
    <div className="App">
      <h1>Notifications</h1>
      <div className="main">      
      {clicked ? data.map((item)=>{
        return(
        <Notify
        name ={item.name}
        address={item.address}
        issue={item.issue}/>
        )
      }) :
      data.slice(0,4).map((item)=>{
        return(
        <Notify
        name ={item.name}
        address={item.address}
        issue={item.issue}/>
        )
      })
      }
      </div>
      <div className='more-btn'>
        {!clicked && <Button type="primary" size='small' onClick={()=>{setClicked(true)}}>more</Button>}
      </div>
    </div>
  );
}

export default App;

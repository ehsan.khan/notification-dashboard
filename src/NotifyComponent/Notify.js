import React,{useState} from 'react';
import { Card } from 'antd';
import { Button,message } from 'antd';

export const Notify = ({name,address,issue}) => {
   const[clicked,setClicked] = useState(false);
   const success = () => {
    message.success('Accepted');
    setClicked(!clicked);
  };
  const error = () => {
    message.error('Rejected');
    setClicked(!clicked);
  };

    return ( 
    <div className='card-body'>
        {(!clicked) && 
           <div className="site-card-border-less-wrapper">
           <Card hoverable={true} style={{ width: 1200}} bordered={false}>
           <div className="card-contents">
           <div className="content">
           <p>{name}</p>
           <p>{address}</p>
           <p>{issue}</p>
           </div>
           <div className="buttons">
           <Button type="primary"   onClick={success}>Accept</Button>
           <Button type="secondary"   onClick={error}>Reject</Button>
           </div>
           </div>  
           </Card>
           </div>
        }
        </div>
    )
}
